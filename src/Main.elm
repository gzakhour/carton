module Main exposing (..)

import AnimationFrame
import Keyboard
import Html exposing (..)
import Dict
import Tuple exposing (first, second)
import Time exposing (Time)

import DataStructures.Core.Graph as Graph exposing (makeNode,addNode,emptyGraph,addEdge,EdgeDirection(IN,OUT,IN_OUT))
import DataStructures.Core.TemporalGraph as TemporalGraph exposing (..)
import DataStructures.Drawables.DrawableGraph exposing (DrawableGraph, fromGraph)
import DataStructures.Drawables.DrawableTemporalGraph exposing (DrawableTemporalGraph, fromTemporalGraph)
import DataStructures.Drawables.DrawableGraphPhysics exposing (updateStep)
import GraphDrawers.GraphDrawer exposing (drawGraph)
import GraphDrawers.TemporalGraphDrawer exposing (drawTgGraph, drawTgTimeline)


nodes : List (Int, String)
nodes = List.range 1 23 |> List.map (\n -> (n, ""))

nodeNameMap : Dict.Dict Int String
nodeNameMap =
    nodes
    |> List.foldr (\node -> \dict ->
        Dict.insert (first node)
                    ((second node) ++ (toString (first node))) dict)
                    Dict.empty

radiusComputer : Int -> Float
radiusComputer n =
    (2.5 * (toFloat n)) + 10


graph : DrawableTemporalGraph
graph = emptyTemporalGraph
    |> addEvent (makeLengthyEvent 5 (AddNode (makeNode 1 "1")))
    |> addEvent (makeLengthyEvent 2 (AddNode (makeNode 2 "2")))
    |> addEvent (makeLengthyEvent 1 (Simultaneous [ AddNode (makeNode 3 "3")
                                                  , AddNode (makeNode 4 "4")] ))
    |> addEvent (makeLengthyEvent 1 (AddEdge (2, "a") 4))
    |> addEvent (makeLengthyEvent 2 (AddNode (makeNode 5 "5")))
    |> addEvent (makeLengthyEvent 3 (Simultaneous [ AddEdge (1, "a") 2
                                                  , AddEdge (3, "a") 4
                                                  , AddEdge (1, "a") 5] ))
    |> addEvent (makeLengthyEvent 1 (Simultaneous [ (AddNode (makeNode 6 "6"))
                                                  , (AddEdge (6, "c") 2) ]))
    |> addEvent (makeLengthyEvent 1 (AddBidirectionalEdge 6 "d" 5))
    |> addEvent (makeLengthyEvent 1 (Simultaneous [ AddEdge (3, "e") 6
                                                  , AddEdge (2, "f") 3]))
    |> addEvent (makeLengthyEvent 3 (RemoveNode 3))
    |> addEvent (makeLengthyEvent 2 (RemoveEdge 1 "a" 2))
    |> addEvent (makeLengthyEvent 2 (RemoveBidirectionalEdge 6 "d" 5))
    |> fromTemporalGraph
    |> advanceTemporalGraph


type alias Model =
    { graph: DrawableTemporalGraph, play: Bool }

init : (Model, Cmd Msg)
init = ({graph=graph,play=True}, Cmd.none)


type Msg
    = KeyMsg Keyboard.KeyCode
    | Tick Time

view : Model -> Html msg
view model =
    div [ ]
        [ drawTgGraph model.graph { width = 800, height = 500 } nodeNameMap
        , drawTgTimeline model.graph { width = 800, height = 100 } ]


updateModelGraph : Bool -> Model -> Model
updateModelGraph check model =
    if check && (not model.play) then model
    else {model|graph=fmapTgGraph (updateStep IN_OUT radiusComputer 1.8) model.graph}


mapGraphAndUpdate : (DrawableTemporalGraph -> DrawableTemporalGraph) -> Model -> (Model, Cmd Msg)
mapGraphAndUpdate func model =
    (updateModelGraph False {model|graph=func model.graph}, Cmd.none)


update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
    case msg of
        -- RIGHT ARROW
        KeyMsg 39 -> mapGraphAndUpdate advanceTemporalGraph model
        -- LEFT ARROW
        KeyMsg 37 -> mapGraphAndUpdate regressTemporalGraph model
        -- SPACE
        KeyMsg 32 -> ({model|play=not model.play}, Cmd.none)
        -- OTHER
        KeyMsg _ -> (model, Cmd.none)
        -- Renderer
        Tick _ -> (updateModelGraph True model, Cmd.none)

subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ Keyboard.downs KeyMsg
        , AnimationFrame.times Tick
        ]

main =
    program
    { init = init
    , view = view
    , update = update
    , subscriptions = subscriptions
    }
