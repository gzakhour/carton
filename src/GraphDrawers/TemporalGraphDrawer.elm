module GraphDrawers.TemporalGraphDrawer exposing (drawTgGraph, drawTgTimeline)

import Svg exposing (..)
import Svg.Attributes exposing (..)

import DataStructures.Core.TemporalGraph exposing (..)
import DataStructures.Core.Vector exposing (..)
import DataStructures.Drawables.DrawableGraph exposing (..)
import DataStructures.Drawables.DrawableTemporalGraph exposing (..)
import GraphDrawers.GraphDrawer exposing (..)


-- DO NOT EXPOSE THIS TYPE
-- We want to keep the guarantee that any nodes to draw will have to go through
-- tgGraphToGeometricLEvents
type alias GeometricLEvent =
    { event : Event Geometry
    , length : Int
    , pos : Int
    , radius : Float }


{-| Draws a light red background from the node till the start of the next one
    spanning all the height of the canvas
-}
colorCurrentTime : Maybe GeometricLEvent -> DrawingCanvas -> List (Svg msg)
colorCurrentTime event canvas =
    case event of
        Just event -> 
            let xcoord = event.pos - (round event.radius) in
            [ rect [ x ((xcoord - 6) |> toString)
                   , y "0"
                   , width (20 * event.length |> toString)
                   , height (canvas.height |> toString)
                   , fill "#ffb8be"
                   , class "currentTimeHighlighter" ]
                   []
             , line [ x1 (xcoord + (round event.radius) |> toString)
                    , x2 (xcoord + (round event.radius) |> toString)
                    , y1 "0"
                    , y2 (toString canvas.height)
                    , strokeWidth "3"
                    , stroke "#ff1e25"
                    , class "currentTimeMarker" ]
                    []]
        Nothing -> []
    

{-| Draws a line between two successive events in the list provided
-}
connectPointers : List GeometricLEvent -> List (Svg msg)
connectPointers events =
    List.map2 (,) events (List.drop 1 events)
    |> List.map (\(event1, event2) ->
        line [ x1 (event1.pos |> toString)
             , x2 (event2.pos |> toString)
             , y1 (13 |> toString)
             , y2 (13 |> toString)
             , strokeWidth "1"
             , stroke "#000"
             , class "timelineConnector" ]
             [])


{-| Draws the nodes indicating the particular points in the timeline
-}
drawPointers : List GeometricLEvent -> DrawingCanvas -> List (Svg msg)
drawPointers events canvas =
    let getEventTypeClass = \event ->
        case event of
            AddNode _ -> "addNode"
            RemoveNode _ -> "removeNode"
            AddEdge _ _ -> "addEdge"
            RemoveEdge _ _ _ -> "removeEdge"
            AddBidirectionalEdge _ _ _ -> "addBidirectionalEdge"
            RemoveBidirectionalEdge _ _ _ -> "removeBidirectionalEdge"
            Simultaneous _ -> ""
    in
    let circleMaker = \index -> \className -> \canvas -> \event ->
        circle [ cx (event.pos |> toString)
               , cy ((event.radius + (index * 30) + 20) / 2 |> toString)
               , r (round event.radius |> toString)
               , fill "#ffffff"
               , strokeWidth "2"
               , class className
               , stroke "#000000" ]
               []
    in
    let drawSeperator = \canvas -> \event ->
        let xcoord = event.pos + 20*event.length - 6 - (round event.radius) in
        line [ x1 (toString xcoord)
             , x2 (toString xcoord)
             , y1 "0"
             , y2 (toString canvas.height)
             , stroke "#999999"
             , strokeWidth "1"
             , class "timelineSeperator" ]
             []
    in
    events
    |> List.map (\point ->
        let event = point.event in
        case event of
            Simultaneous events ->
                List.indexedMap (\i -> \e ->
                    circleMaker (toFloat i) (getEventTypeClass e) canvas point) events
                |> (::) (drawSeperator canvas point)
            _ -> [ drawSeperator canvas point
                 , circleMaker 0 (getEventTypeClass event) canvas point ])
    |> List.concat
        


{-| Convert a temporal graph to a list of geometric events that can be drawn.
    It's important to not expose another function that produces a GeometricLEvent
-}
tgGraphToGeometricLEvents : DrawableTemporalGraph -> List GeometricLEvent
tgGraphToGeometricLEvents tgraph =
    -- Map each timeline event to a point
    -- I fear we must reduce a list to a list!
    getTemporalGraphTimeline tgraph
    |> List.foldl (\event -> \(start,list) ->
                      ( start+20*event.length
                      , { event = flattenEvent event.event
                        , length = event.length
                        , pos = start+12
                        , radius = 5}::list))
                  (0,[])
    |> Tuple.second
    |> List.reverse



{-| Draws the timeline of a temporal graph
-}
drawTgTimeline : DrawableTemporalGraph -> DrawingCanvas -> Svg msg
drawTgTimeline tgraph canvas =
    let timeline = tgGraphToGeometricLEvents tgraph in
    let currentEvent = List.drop ((getTemporalGraphTime tgraph) - 1) timeline |> List.head in
    svg [ version "1.1"
        , x "0"
        , y "0"
        , width (toString canvas.width)
        , height (toString canvas.height) ]
        (List.concat [ colorCurrentTime currentEvent canvas
                     , connectPointers timeline
                     , drawPointers timeline canvas ])


{-| Draws the graph of a temporal graph
-}
drawTgGraph : DrawableTemporalGraph -> DrawingCanvas -> NodeNameMap -> Svg msg
drawTgGraph tgraph =
    drawGraph (getTgGraph tgraph)
