module GraphDrawers.GraphDrawer exposing (..)

import Svg exposing (..)
import Svg.Attributes exposing (..)
import List
import Dict
import Tuple exposing (first, second)
import String

import DataStructures.Drawables.DrawableGraph exposing (..)
import DataStructures.Core.Graph exposing (..)
import DataStructures.Core.Vector exposing (..)


---
--- Types
---

type alias DrawingCanvas = {
    width: Float,
    height: Float
}
type alias Boundary = {
    minX: Float,
    minY: Float,
    maxX: Float,
    maxY: Float
}

type alias NodeNameMap = Dict.Dict NodeId String

---
--- Graph to SVG converters
---

{-| Provided with a graph, this function will return an SVG element for each
    node in the graph. This just draws the node without the lable, refer to
    getNodeLabelsElement for the label drawer.
-}
getNodeElements : DrawableGraph -> List (Svg msg)
getNodeElements graph =
    let radius = \node -> (getNodeVal node).radius in
    let disp = \node -> (getNodeVal node).disp in
    List.map
        (\node -> circle [fill "#505050",
                          r (radius node |> toString),
                          cx (node |> disp |> first |> toString),
                          cy (node |> disp |> second |> toString),
                          class "nodeCircle"] [])
        (getNodes graph)


{-| Provided with a mapper from node IDs to node labels, this function returns
    an SVG element for each node's label in the graph. This does not draw the
    node itself, just the label, refer to getNodeElements for the node producer.
-}
getNodeLabelsElements : Dict.Dict Int String -> DrawableGraph -> List (Svg msg)
getNodeLabelsElements nodeNameMap graph =
    List.map (\node ->
                    let disp = (getNodeVal node).disp in
                    let radius = (getNodeVal node).radius in
                    let id = getNodeId node in
                    text_ [x (first disp |> toString),
                              y ((second disp) + radius + 15 |> toString),
                              textAnchor "middle",
                              class "nodeLabel" ]
                             [Dict.get id nodeNameMap |> Maybe.withDefault "N/A" |> text])
             (getNodes graph)



{-| Provided with an edge, i.e. the start node, the target node, and the edge
    label, together with the index in and the size of the set of edges going
    from start to target, this function returns an SVG element containing the
    label.
-}
getEdgeLabelElement : Int -> Int -> VectorPhys -> VectorPhys -> String -> Svg msg
getEdgeLabelElement index size start target label =
    let midpoint = scalarVector 0.5 (addVectors start target) in
    let angle = getAngle (\x -> \y -> atan (x/y)) start target in
    text_ [x (first midpoint |> toString),
           y (second midpoint |> toString),
           textAnchor "middle",
           transform ("rotate("++(toString angle)++
                               ","++(first midpoint |> toString)++
                               ","++(second midpoint |> toString)++")"),
           fill "#cc5050",
           class (label ++ " edgeLabel")]
          [ label |> text]


{-| Provided with an edge (ref to getEdgeLabelElement) for an explanation of
    the meaning of the arguments) this function returns an SVG element to draw
    the edge.
-}
getEdgeElement : Int -> Int -> VectorPhys -> VectorPhys -> String -> Svg msg
getEdgeElement index size start target label =
    let dist = (distance2 start target) |> sqrt in
    let (i, n) = map2 toFloat (index, size) in
    let vec2str = \vec -> (first vec |> toString) ++ " " ++ (second vec |> toString) in
    let rx = dist / 2 in
    let ry = 20 * (2 * i + 1 - n |> abs) / n in
    let xAxisRot = getAngle Basics.atan2 start target in
    let longerArcFlag = 0 in
    let sweepFlag = if 2 * index < size then 0 else 1 in
    let minStart = Basics.min start target in
    let maxTarget = Basics.max start target in
    Svg.path
        [ d ("M "++(vec2str minStart)++
             " A "++(String.join  " " (List.map toString [rx, ry, xAxisRot, longerArcFlag, sweepFlag]))
                  ++" "++(vec2str maxTarget))
         , stroke "#cc5050"
         , strokeWidth "1"
         , fill "transparent"
         , class (label ++ " edge")]
        []


{-| Provided with an edge (ref to getEdgeElement for meaning of arguments) and
    the size of the arrow, this function will return an SVG element for the
    triangle indicating the orientation of an edge.
-}
getEdgeArrowElement : Int -> Int -> Float -> VectorPhys -> VectorPhys -> String -> Int -> Svg msg
getEdgeArrowElement index size radius start target label width =
    let (i, n) = map2 toFloat (index, size) in
    let ry = 20 * (2 * i + 1 - n |> abs) / n in
    let angle = (getAngle atan2 start target) - 90 in
    let sweepFlag = if 2 * index < size then 1 else -1 in
    let midpoint = addVectors target start |> scalarVector 0.5 in
    let point = subVectors target (subVectors target start |> normalize |> scalarVector (radius + (toFloat width) / 3.33)) in
    let curvOffset = subVectors target start |> getOrthonormal |> scalarVector ry in
    -- the coordinates of a unit equilateral triangle
    let (x0, x1, x2) = ("0 0", "1 0", "0.5 0.886") in
    polygon [ points (String.join "," [x0, x1, x2])
            , fill "#cc5050"
            , transform ( ""
                        ++ "translate("++((if n > 1 then (addVectors midpoint curvOffset) else point) |> commaVect)++") "
                        ++ "rotate("++(toString angle)++") "
                        ++ "scale(" ++ (toString width) ++ ") "
                        ++ "translate(-0.5,-0.433) "
                        )
            , strokeWidth "1"
            , class (label ++ " edgeArrow")
            ]
            [ ]


insertIf : Bool -> a -> List a -> List a
insertIf cond elm lst =
    if cond then elm::lst else lst

{-| Given a graph, this function returns a list of svg elements for each edge
    where each element is responsible for drawing said edge.
-}
getEdgeElements : DrawableGraph -> List (Svg msg)
getEdgeElements graph =
    let disp = \node -> (getNodeVal node).disp in
    getNodes graph
    |> List.map (\node1 -> List.map (\node2 -> (node1, node2)) (getNeighbors graph node1))
    |> List.concat
    |> groupEdgesByEnds
    |> List.map (\edges ->
        List.indexedMap (,) edges
        |> List.map (\x -> (first x, List.length edges, second x))
        |> List.map (\(index,size,edge) ->
            let start = edge |> first |> disp in
            let target = edge |> second |> first |> disp in
            let label = edge |> second |> second in
            let radius = (getNodeVal (edge |> second |> first)).radius in
            [getEdgeElement index size start target label]
            |> (::) (getEdgeArrowElement index size radius start target label 10)
            |> insertIf (size < 2) (getEdgeLabelElement index size start target label)
            )
        |> List.concat)
    |> List.concat


{-| Find the boundary point of a graph alongside an axis. That is, project the
    coordinates of the nodes on the provided axis (0 is the x-axis and 1 is the
    y-axis), then apply the provided function (meant to be List.minimum or
    List.maximum), with a default of 0
-}
getBoundaryPoint : (List Float -> Maybe Float) -> Int -> DrawableGraph -> Float
getBoundaryPoint func axis graph =
    let projection = \axis -> if axis == 0 then first else second in
    getNodes graph
    |> List.map (\x -> projection axis (getNodeVal x).pos)
    |> func
    |> Maybe.withDefault 0


{-| Given a graph, this function returns the boundaries of the smallest box that
    can fit all of the graph.
-}
getGraphBoundary : DrawableGraph -> Boundary
getGraphBoundary graph =
    {
        minX = getBoundaryPoint List.minimum 0 graph,
        maxX = getBoundaryPoint List.maximum 0 graph,
        minY = getBoundaryPoint List.minimum 1 graph,
        maxY = getBoundaryPoint List.maximum 1 graph
    }


{-| Given a boundary and a canvas followed by a node, this function will produce
    a node that lives in the canvas. The mapped node and the original one are
    located "in the same place" with respect to the boundaries.

    That is, this function will stretch and scale the boundary to fit the canvas.
-}
fitNodeInCanvas : Boundary -> DrawingCanvas -> DrawableNode -> DrawableNode
fitNodeInCanvas boundary canvas node =
    let geom = getNodeVal node in
    let newX = ((canvas.width - 50) / (boundary.maxX - boundary.minX))
                * ((first geom.pos) - boundary.minX) + 25 in
    let newY = ((canvas.height - 50)/ (boundary.maxY - boundary.minY))
                * ((second geom.pos) - boundary.minY) + 25 in
    makeNode (getNodeId node) {geom|disp=(newX, newY)}


{-| Exactly like fitNodeInCavas except with graphs.
-}
fitGraphInDrawingCanvas : DrawingCanvas -> DrawableGraph -> DrawableGraph
fitGraphInDrawingCanvas canvas graph =
    mapNodes (fitNodeInCanvas (getGraphBoundary graph) canvas) graph


{-| Produce an SVG that draws the graph.
-}
drawGraph : DrawableGraph -> DrawingCanvas -> NodeNameMap -> Svg msg
drawGraph graph canvas nodeNameMap =
    let fitGraph = fitGraphInDrawingCanvas canvas graph in
    svg [ version "1.1", x "0", y "0", width (toString canvas.width), height (toString canvas.height) ]
        (List.concat [getEdgeElements fitGraph,
                      getNodeElements fitGraph,
                      getNodeLabelsElements nodeNameMap fitGraph])

