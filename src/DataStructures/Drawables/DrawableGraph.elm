module DataStructures.Drawables.DrawableGraph exposing (..)

import Tuple exposing (..)
import Random exposing (float,int)
import Html exposing (..)
import Dict

import DataStructures.Core.Vector exposing (..)
import DataStructures.Core.Graph exposing (..)



---
--- Types
---
type alias VectorPhys = Vector Float

type alias Geometry =
    {
        pos : VectorPhys,   -- the position on the 2D plane
        vel : VectorPhys,   -- the velocity at the last update
        force : VectorPhys, -- the force at the node
        disp : VectorPhys,  -- the position on the canvas
        radius: Float       -- the radius of the node
    }


type alias DrawableGraph = Graph Geometry
type alias DrawableNode = Node Geometry



---
--- Generators
---

{-| Create a random geometry generator

    randomGeometryGen -42.0 10.5    -- a random point in [-42,10.5]x[-42,10.5]
-}
randomGeometryGen : Float -> Float -> Random.Generator Geometry
randomGeometryGen minPos maxPos = 
    let genPos = float minPos maxPos in
    Random.map2 (\a -> \b ->
                    {
                        pos = (a, b),
                        disp = (a, b),
                        vel = (0, 0),
                        force = (0, 0),
                        radius = 0
                    })
                genPos genPos

{-| Create a random geometry seeded by a node

    randomGeomFromNode (Node 10 "node")    -- a point in [0,500]^2
-}
randGeomFromNode : Node a -> Geometry
randGeomFromNode node =
    Random.initialSeed (getNodeId node)
    |> Random.step (randomGeometryGen 0 500)
    |> Tuple.first


{-| Create a random drawable graph from any given graph. This function will create
    a graph identical in structure to the provided graph with every node's data
    replaced by the geometry of the node.
-}
fromGraph : Graph a -> DrawableGraph
fromGraph graph =
    mapNodes (\node -> makeNode (getNodeId node) (randGeomFromNode node))
             graph 


{-| A constructor for the DrawableNode type.
    
    The id must be unique for every node in the graph.
-}
makeGeomNode : Int -> VectorPhys -> VectorPhys -> VectorPhys -> VectorPhys -> Float -> DrawableNode
makeGeomNode id pos vel force disp radius =
    makeNode id { pos = pos, disp = disp, force = force, vel = vel, radius = radius }



{-| Given a list of edges this function will group all the edges having the
    same direction or the inverse direction. That, given an edge e1 going from
    start1 to target1 and e2 going from start2 to target2, then e1 and e2 are
    goruped together iff {start1,target1} == {start2,target2} (== is set equality)
-}
groupEdgesByEnds : List (Node Geometry, (Node Geometry, String))
                -> List (List (Node Geometry, (Node Geometry, String)))
groupEdgesByEnds edges =
    -- we need to group edges by {start,target}. I.e. group by edges regardless
    -- of their direction. To do that I need a 1-1 symmetric function f
    -- f : Node Geometry -> Node Geometry -> comparable
    -- that is 1-1 and symmetric. That function will be used to generate the keys
    -- of the dictionary storing the groups
    let f = \sId-> \tId ->
        (Basics.min sId tId, Basics.max sId tId) in
    let update = \dict -> \key -> \edge ->
         dict
         |> Dict.get key
         |> Maybe.withDefault []
         |> (::) edge
         |> \value -> Dict.insert key value dict in
    let sId = \e -> first e |> getNodeId in
    let tId = \e -> second e |> first |> getNodeId in
    List.foldr (\e -> \dict ->
                   let key = f (first e |> getNodeId) (second e |> first |> getNodeId) in
                   update dict key e)
               Dict.empty
               edges
    |> Dict.values





---
--- Drawable Graph Util functions
---

{-| Locate the center of the graph. If the DrawableNodes of a graph are located
    at [(xi, yi)]_1^n then the returned value is (1.0/n)*(sum_1^n xi, sum_1^n yi)
-}
getGraphCenter : DrawableGraph -> VectorPhys
getGraphCenter graph =
    let nodes = graph |> getNodes in
    List.map (\node -> (getNodeVal node).pos) nodes
    |> List.foldr addVectors (0, 0)
    |> scalarVector (1.0/(List.length nodes |> toFloat))
