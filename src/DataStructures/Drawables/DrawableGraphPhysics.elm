module DataStructures.Drawables.DrawableGraphPhysics exposing (..)

import DataStructures.Core.Graph exposing (..)
import DataStructures.Drawables.DrawableGraph exposing (..)
import DataStructures.Core.Vector exposing (..)


{-| Compute the repulsive force that the other node puts on the self node.
    The force is similar to Coulomb's law.
    The force grows lineariy with the radius of the other node's radius.
-}
repulsiveForceBetween : DrawableNode -> DrawableNode -> VectorPhys
repulsiveForceBetween other self =
    let sval = getNodeVal self in
    let oval = getNodeVal other in
    let g = -400 * oval.radius / (distance2 sval.pos oval.pos) in
    scalarVector g (normalize (subVectors sval.pos oval.pos))


{-| Compute the attractive force that the other node puts on the self node.
    The force is similar to Hook's law.
-}
attractiveForceBetween : DrawableNode -> DrawableNode -> VectorPhys
attractiveForceBetween other self =
    let selfpos = (getNodeVal self).pos in
    let otherpos = (getNodeVal other).pos in
    let k = 0.005 * (sqrt (distance2 selfpos otherpos)) in
    scalarVector k (normalize (subVectors selfpos otherpos))


{-| Compute the force attracting the node to the center.

    Imagine a graph with two disconnected components. Then there will always be
    a repulsive force between both components that is unrestricted, leading
    the graph's diameter to grow infinetly.

    With a force pulling the nodes to the center of the graph then the graph's
    diameter will stabilize at some point
-}
containingAttractiveForce : VectorPhys -> DrawableNode -> VectorPhys
containingAttractiveForce center node =
    let pos = (getNodeVal node).pos in
    let dir = subVectors center pos |> normalize in
    let dist = sqrt (distance2 center pos) in
    scalarVector (0.005 * dist) dir


{-| Compute the overall force that node1 exerts on node2 in the
    context of the graph.
-}
forceBetween : DrawableGraph -> DrawableNode -> DrawableNode -> VectorPhys
forceBetween graph node1 node2 =
    let connected = isConnected node1 node2 graph || isConnected node2 node1 graph in
    let repulsive = repulsiveForceBetween node1 node2 |> clampVec -10 10 in
    let attractive = if connected then
        attractiveForceBetween node1 node2 |> clampVec -10 10 else (0, 0) in
    addVectors repulsive attractive


{-| Compute the overall force applied on the node in the
    context of the graph
-}
getForce : DrawableGraph -> DrawableNode -> VectorPhys
getForce graph node =
    let center = getGraphCenter graph in
    graph
    |> getNodes
    |> List.filter (\x -> not (getNodeId x == getNodeId node))
    |> List.map (\other -> forceBetween graph node other)
    |> List.foldr addVectors (containingAttractiveForce center node)


{-| Compute the new position of the node for the next frame. timeStep is the
    length of the discretized timed. vectorVel is the velocity of the node at
    that point
    
    The update rule is:
    pos' = vel * timeStep + pos
-}
updatePos : Float -> DrawableNode -> VectorPhys -> VectorPhys
updatePos timeStep node vectorVel =
    let geometry = getNodeVal node in
    vectorVel |> scalarVector timeStep |> addVectors geometry.pos


{-| Compute the new velocity of the node for the next frame. timeStep is the
    length of the discretized timed. force is the force applied on the node at
    the moment.

    The update rule is:
    vel' = timeStep * force
-}
updateVel : Float -> DrawableNode -> VectorPhys -> VectorPhys
updateVel timeStep node force =
    let geometry = getNodeVal node in
    force |> scalarVector timeStep


{-| Update the display position.
    Not much here folks.
-}
updateDisp : VectorPhys -> VectorPhys
updateDisp =
    identity


{-| Update the node of a graph.
    
    Refer to updateStep for documentation
-}
updateNodeMapper : EdgeDirection -> (Int -> Float) -> Float -> DrawableGraph -> DrawableNode -> DrawableNode
updateNodeMapper edgeDirType radiusComputer timeStep graph node =
    let force = getForce graph node in
    let updatedVel = updateVel timeStep node force in
    let updatedPos = updatePos timeStep node updatedVel in
    let updatedDisp = updateDisp updatedPos in
    makeGeomNode (getNodeId node) updatedPos updatedVel force updatedDisp
                 (radiusComputer (getNumNeighbors edgeDirType node graph))
    


{-| Update the graph by one step.
    
    edgeDirType determines what node drawer strategy to use. Possible values are
    IN (for incoming edges), OUT (for outgoing edges), and IN_OUT (for both).
    radiusComputer is the function that takes the number of edgeDirType edges
    and outputs the radius used for the nodes.

    I recommend using
    \n -> 15 * Base.logBase 10 (n + 2)
    for radiusComputer
-}
updateStep : EdgeDirection -> (Int -> Float) -> Float -> DrawableGraph -> DrawableGraph
updateStep edgeDirType radiusComputer timeStep graph =
    mapNodes (updateNodeMapper edgeDirType radiusComputer timeStep graph) graph
