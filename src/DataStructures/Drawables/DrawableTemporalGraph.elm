module DataStructures.Drawables.DrawableTemporalGraph exposing (..)

import DataStructures.Core.Graph exposing (..)
import DataStructures.Core.TemporalGraph exposing (..)
import DataStructures.Drawables.DrawableGraph exposing (..)

type alias DrawableTemporalGraph = TemporalGraph Geometry


{-| Provided with a TemporalGraph, produce a DrawableTemporalGraph
-}
fromTemporalGraph : TemporalGraph a -> DrawableTemporalGraph
fromTemporalGraph graph =
    fmapTemporalGraph (\node -> makeNode (getNodeId node) (randGeomFromNode node))
              graph
