module DataStructures.Core.Graph exposing (
    Node,
    NodeId,
    Graph,
    EdgeDirection(IN,OUT,IN_OUT),
    OutgoingEdge,
    EdgeLabel,
    emptyGraph,
    getNodeId,
    getNodeVal,
    getNodes,
    getNeighbors,
    getNumNeighbors,
    addNode,
    removeNode,
    addEdge,
    removeEdge,
    addBidirectionalEdge,
    removeBidirectionalEdge,
    mapNodes,
    makeNode,
    isConnected,
    isConnectedId)


import Html
import Dict
import Set exposing (toList)
import Maybe exposing (withDefault)
import Tuple


---
--- Types and generators
---

type alias NodeId = Int
type alias EdgeLabel = String

type Node a = Node NodeId a
            | ImpossibleNode -- DO NOT EXPOSE THIS CONSTRUCTOR --

type alias OutgoingEdge = (NodeId, EdgeLabel) -- (nodeId, edgeLabel)

type alias Graph a = {
    nodes : Dict.Dict NodeId (Node a),
    nodeIds : Set.Set NodeId,
    edges : Dict.Dict Int (Set.Set OutgoingEdge)
}

type EdgeDirection =
    IN | OUT | IN_OUT



{-| Create a node with a given id storing the given data
-}
makeNode : NodeId -> a -> Node a
makeNode id value =
    Node id value

---
--- Helpful Constants
---

emptyNodes : Dict.Dict NodeId (Node a)
emptyNodes = Dict.empty
emptyNodeIds : Set.Set NodeId
emptyNodeIds = Set.empty
emptyEdges : Dict.Dict NodeId (Set.Set OutgoingEdge)
emptyEdges = Dict.empty

-- DO NOT EXPOSE THIS CONSTANT --
impossibleNode : Node a
impossibleNode = ImpossibleNode

-- Empty graph
emptyGraph : Graph a
emptyGraph = {
    nodes = emptyNodes,
    nodeIds = emptyNodeIds,
    edges = emptyEdges }



---
--- Node Operations
---

{-| Get the id of a node

    getNodeId (Node 10 "node") == 10
-}
getNodeId : Node a -> NodeId
getNodeId node =
    case node of
        Node x a -> x
        ImpossibleNode -> getNodeId node -- !! undefined behavior !!



{-| Get the data stored in a node
    
    getNodeVal (Node 42 "node") == "node"
-}
getNodeVal : Node a -> a
getNodeVal node =
    case node of
        Node x a -> a
        ImpossibleNode -> getNodeVal node -- !! undefined behavior !!



---
--- Graph Operations
---

{-| Get a list of all the nodes in a graph

    getNodes (emptyGraph |> addNode (Node 42 "node")) == [Node 42 "node"]
-}
getNodes : Graph a -> List (Node a)
getNodes graph =
    graph.nodes |> Dict.values


{-| Query the graph for a node by id. If the id does not exist then a value
    of type ImpossibleNode is returned. A value cannot be constructed outside
    of this module.

    getNodeById (emptyGraph |> addNode (Node 42 "node")) 42 == (Node 42 "node")
-}
getNodeById : Graph a -> NodeId -> Node a
getNodeById graph id =
    Dict.get id graph.nodes |> withDefault impossibleNode


{-| Find the number of outgoing edges from a node.
-}
getNumNeighborsOut : Node a -> Graph a -> Int
getNumNeighborsOut node graph =
    withDefault Set.empty (Dict.get (getNodeId node) graph.edges)
    |> Set.size


{-| Find the number of incoming edges to a node.
-}
getNumNeighborsIn : Node a -> Graph a -> Int
getNumNeighborsIn node graph =
    let id = getNodeId node in
    graph.edges
    |> Dict.map (\_ -> \edges -> Set.filter (\e -> (Tuple.first e) == id) edges)
    |> Dict.values
    |> List.map Set.size
    |> List.foldr (+) 0


{-| Find the number of the edges whose direction is indicated by dir.
-}
getNumNeighbors : EdgeDirection -> Node a -> Graph a -> Int
getNumNeighbors dir node graph =
    let in_ = getNumNeighborsIn node graph in
    let out_ = getNumNeighborsOut node graph in
    case dir of
        IN -> in_
        OUT -> out_
        IN_OUT -> in_ + out_


{-| Get a list of all the edges a node has.
    If the node was not found then an empty list is returned.
-}
getNeighbors : Graph a -> Node a -> List (Node a, String)
getNeighbors graph node =
    withDefault Set.empty (Dict.get (getNodeId node) graph.edges)
    |> Set.toList
    |> List.map (Tuple.mapFirst (getNodeById graph))


{-| Checks if the start node is connected through an edge
    to the target node
-}
isConnected : Node a -> Node a -> Graph a -> Bool
isConnected start target graph =
    isConnectedId (getNodeId start) (getNodeId target) graph


{-| Checks if the node with the id start is connected through an edge
    to the node with the id target
-}
isConnectedId : NodeId -> NodeId -> Graph a -> Bool
isConnectedId start target graph =
    Dict.get start graph.edges
    |> Maybe.map (\edges ->
        Set.map Tuple.first edges |> Set.member target)
    |> withDefault False



---
--- Graph Transformers
---
{-| Add a node to a graph

    -- Create a graph with 23 nodes
    List.range 1 23
    |> List.map (\n -> (n, ""))
    |> List.map (\x -> makeNode (first x) (second x))
    |> List.foldr addNode emptyGraph
-}
addNode : Node a -> Graph a -> Graph a
addNode node graph =
    let id = getNodeId node in
        if (Set.member id graph.nodeIds) then
            graph
        else
            { nodes = Dict.insert id node graph.nodes,
              nodeIds = Set.insert id graph.nodeIds,
              edges = graph.edges }


removeNode : NodeId -> Graph a -> Graph a
removeNode nodeId graph =
    { nodes = Dict.remove nodeId graph.nodes
    , nodeIds = Set.remove nodeId graph.nodeIds
    , edges = Dict.remove nodeId graph.edges -- delete all outgoing edges
           -- then delete all incoming edges
           |> Dict.map (\key -> \edges ->
                Set.filter (\edge -> Tuple.first edge /= nodeId) edges)
    }


{-| Add an edge connecting the startId node to the targetId node with
    a provided label
-}
addEdge : NodeId -> EdgeLabel -> NodeId -> Graph a -> Graph a
addEdge targetId relType startId graph =
    if (Set.member targetId graph.nodeIds) && (Set.member startId graph.nodeIds) then
       {graph|edges=Dict.update startId
            (\value -> case value of
                Nothing -> Set.insert (targetId, relType) Set.empty |> Just
                Just edges -> edges |> Set.insert (targetId, relType) |> Just)
            graph.edges
       }
    else
        graph


removeEdge : NodeId -> EdgeLabel -> NodeId -> Graph a -> Graph a
removeEdge targetId relType startId graph =
    { graph
    | edges = Dict.map (\key -> \edges ->
        Set.filter (\edge -> (edge /= (targetId, relType)) || (key /= startId)) edges)
        graph.edges }


{-| Just like addEdge, but done twice. An edge is added for both directions.
-}
addBidirectionalEdge : NodeId -> EdgeLabel -> NodeId -> Graph a -> Graph a
addBidirectionalEdge targetId relType startId graph =
    addEdge targetId relType startId graph
    |> addEdge startId relType targetId


removeBidirectionalEdge : NodeId -> EdgeLabel -> NodeId -> Graph a -> Graph a
removeBidirectionalEdge targetId relType startId graph =
    (removeEdge targetId relType startId >> removeEdge startId relType targetId)
        graph



{-| The most useful transform. Given a function that maps nodes, this function
    will produce an equivalent graph with its nodes mapped.
-}
mapNodes : (Node a -> Node b) -> Graph a -> Graph b
mapNodes func graph = {graph|nodes=Dict.map (\key -> \node -> func(node)) graph.nodes}
