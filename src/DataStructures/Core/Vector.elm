module DataStructures.Core.Vector exposing (..)

import Tuple exposing (first, second, mapFirst, mapSecond)


---
--- Types
---

type alias Vector a = (a, a)


---
--- Vector Manipulations
---

{-| add two vectors of numbers component-wise.

    addVectors (-10, 42) (10, 42) == (0, 84)
-}
addVectors : Vector number -> Vector number -> Vector number
addVectors x y =
    (first x + first y, second x + second y)


{-| multiply a vector by a scalar

    scalarVector -0.5 (2, 4) == (-1, -2)
-}
scalarVector : number -> Vector number -> Vector number
scalarVector c x =
    (c * first x, c * second x)


{-| subtract two vectors of numbers component wise

    subVectors (10, 0) (10, 10) == (0, -10)

    In general for any a, b then

    subVector a b == addVector a (scalarVector -1.0 b)
-}
subVectors : Vector number -> Vector number -> Vector number
subVectors x y =
    (first x - first y, second x - second y)


{-| Compute the distance (squared) between two vectors.

    distance2 (0, 0) (3, 4) == 25
-}
distance2 : Vector number -> Vector number -> Float
distance2 vec1 vec2 =
    (Tuple.first vec1 - Tuple.first vec2) ^ 2
    + (Tuple.second vec1 - Tuple.second vec2) ^ 2


{-| Compute the norm of a vector.

    norm (0, -1) == 1

    In general for any vector a then

    norm a == sqrt (distance2 a (0, 0))
-}
norm : Vector number -> Float
norm x =
    sqrt (distance2 x (0, 0))



{-| Normalize a vector. That is, the same input vector but scaled down so its
    norm is 1.

    normalize (1, -1) == (0.707, -0.707)
-}
normalize : Vector Float -> Vector Float
normalize x =
    scalarVector (1.0/(norm x)) x


{-| Returns the orthonormal vector to a given vector a. Since there are two to
    choose from, this function returns the vector b such that

    a x b = (0, 0, 1)
-}
getOrthonormal : Vector Float -> Vector Float
getOrthonormal vec =
    (-1 * (second vec), first vec) |> normalize



{-| apply the same mapping for both components of the vector.

    map2 ((+) 1) (-1, 1) == (0, 2)
    map2 (\n -> sin n) (0, pi/2) == (0, 1)
-}
map2 : (a -> b) -> Vector a -> Vector b
map2 func vec =
    vec |> mapFirst func |> mapSecond func


{-| Clamp the components of a vector between two values

    clampVec 10 20 (15, 16) == (15, 16)
    clampVec 10 20 (-10, 100) == (10, 20)
    clampVec 10 20 (11, 50) == (11, 20)
-}
clampVec : Float -> Float -> Vector Float -> Vector Float
clampVec minVal maxVal =
    map2 (clamp minVal maxVal)


{-| Finds the angle between two vectors using a custom atan strategy.
    The first argument can be any of Basics.atan or Basics.atan2, or something
    else.
-}
getAngle : (Float -> Float -> Float) -> Vector Float -> Vector Float -> Float
getAngle tan_1 start target =
    (tan_1 ((second target) - (second start))
           ((first target) - (first start)))
    * 180.0 / Basics.pi
    

{-| Given a vector, this function outputs a string that is made up of the first
    components and the second component separated by a comma.

    commaVect ("hello", "world") == "hello,world"
    commaVect (0, 1) == "0,1"
-}
commaVect : Vector a -> String
commaVect vec =
    (first vec |> toString) ++ "," ++ (second vec |> toString)
