module DataStructures.Core.TemporalGraph exposing (
    TemporalGraph,
    emptyTemporalGraph,
    makeLengthyEvent,
    Event(..),
    flattenEvent,
    addEvent,
    addEventAt,
    getEventAt,
    getGraphAt,
    fmapEvent,
    fmapLengthyEvent,
    fmapTemporalGraph,
    fmapTgGraph,
    getTemporalGraphAt,
    advanceTemporalGraph,
    regressTemporalGraph,
    getTgGraph,
    getTemporalGraphTime,
    getTemporalGraphTimeline)

import Array
import List
import Tuple exposing (first, second)

import DataStructures.Core.Graph exposing (..)

---
--- Types
---
type Event a =
      AddNode (Node a)
    | RemoveNode NodeId
    | AddEdge OutgoingEdge NodeId
    | RemoveEdge NodeId EdgeLabel NodeId
    | AddBidirectionalEdge NodeId EdgeLabel NodeId
    | RemoveBidirectionalEdge NodeId EdgeLabel NodeId
    | Simultaneous (List (Event a))

type alias LengthyEvent a = {
        event: Event a,
        length: Int
    }

type TemporalGraph a = TemporalGraph (List (LengthyEvent a)) Int (Graph a)


emptyTemporalGraph : TemporalGraph a
emptyTemporalGraph = TemporalGraph [] 0 emptyGraph


---
--- Event generators
---

makeLengthyEvent : Int -> Event a -> LengthyEvent a
makeLengthyEvent length event = {
        event=event,
        length=length
    }


---
--- TemporalGraph transformers and queriers
---
flattenEvent : Event a -> Event a
flattenEvent event =
    case event of
        Simultaneous events -> Simultaneous (List.foldl (\e -> \list ->
                case flattenEvent e of
                    Simultaneous es -> List.concat [es, list]
                    _ -> e::list
            ) [] events)
        _ -> event


{-| Add an event at the end of the graph timeline
-}
addEvent : LengthyEvent a -> TemporalGraph a -> TemporalGraph a
addEvent event tgraph =
    case tgraph of
        TemporalGraph timeline currentTime graph ->
            TemporalGraph (List.reverse (event::List.reverse timeline))
                          currentTime graph


addToListAt : Int -> a -> List a -> List a
addToListAt index elm lst =
    if index >= List.length lst then List.reverse (elm::lst) else
    let (smaller, bigger) = List.partition (\(i,_) -> i < index) (List.indexedMap (,) lst) in
    List.append smaller ((0,elm)::bigger)
    |> List.map second


{-| Add an event at the provided index
-}
addEventAt : Int -> LengthyEvent a -> TemporalGraph a -> TemporalGraph a
addEventAt index event tgraph =
    case tgraph of
        TemporalGraph timeline currentTime graph ->
            TemporalGraph (addToListAt index event timeline)
                          currentTime graph


{-| Retrieve the event at a given index
-}
getEventAt : Int -> TemporalGraph a -> Maybe (LengthyEvent a)
getEventAt index tgraph =
    case tgraph of
        TemporalGraph timeline currentTime graph ->
            case List.drop index timeline of
                [] -> Nothing
                x::xs -> Just x


{-| Given a graph and an event, this function will transform the graph to a
    new one reflecting the changes brought by the event
-}
handleEvent : Event a -> Graph a -> Graph a
handleEvent event graph =
    case event of
        AddNode node -> graph |> addNode node
        RemoveNode nodeId -> graph |> removeNode nodeId
        AddEdge edge startId -> graph |> addEdge (first edge) (second edge) startId
        RemoveEdge tId label sId -> graph |> removeEdge tId label sId
        AddBidirectionalEdge targetId edgeLabel startId ->
            graph |> addBidirectionalEdge targetId edgeLabel startId
        RemoveBidirectionalEdge tId label sId ->
            graph |> removeBidirectionalEdge tId label sId
        Simultaneous events ->
            List.foldl (\event -> \graph -> handleEvent event graph) graph events


{-| Provided with a temporal graph this function produces the graph at a provided
    index.
-}
getGraphAt: Int -> TemporalGraph a -> Graph a
getGraphAt index tgraph =
    case tgraph of
        TemporalGraph timeline currentTime graph ->
            List.foldl (\e -> handleEvent e.event) emptyGraph (List.take index timeline)


{-| An fmap on Event.
-}
fmapEvent : (Node a -> Node b) -> Event a -> Event b
fmapEvent func event =
    case event of
        AddNode node -> AddNode (func node)
        RemoveNode nodeId -> RemoveNode nodeId
        AddEdge edge startId -> AddEdge edge startId
        RemoveEdge tId label sId -> RemoveEdge tId label sId
        AddBidirectionalEdge tId label sId -> AddBidirectionalEdge tId label sId
        RemoveBidirectionalEdge tId label sId -> RemoveBidirectionalEdge tId label sId
        Simultaneous events -> Simultaneous (List.map (\event -> fmapEvent func event) events)


{-| An fmap on LengthyEvent
-}
fmapLengthyEvent : (Node a -> Node b) -> LengthyEvent a -> LengthyEvent b
fmapLengthyEvent func levent =
    {levent|event=fmapEvent func levent.event}


{-| An fmap on TemporalGraph
-}
fmapTemporalGraph : (Node a -> Node b) -> TemporalGraph a -> TemporalGraph b
fmapTemporalGraph func tgraph =
    case tgraph of
        TemporalGraph timeline currentTime graph ->
            TemporalGraph (List.map (fmapLengthyEvent func) timeline)
                          currentTime
                          (mapNodes func graph)


{-| An fmap on TemporalGraph but for the Graph, not the Node
-}
fmapTgGraph : (Graph a -> Graph a) -> TemporalGraph a -> TemporalGraph a
fmapTgGraph func tgraph =
    case tgraph of
        TemporalGraph timeline currentTime graph ->
            TemporalGraph timeline currentTime (func graph)


{-| Get a copy of the provided graph with its state moved to the provided time.

    For example if you had a temporal graph with a timeline being: AddNode node1,
    AddNode node2, AddEdge (node2id, label) node1id; and currentTime = 0 then
    getTemporalGraphAt 3 tgraph
    will produce a copy of tgraph with tgraph.currentTime = 3 and tgraph.graph
    having both nodes and the edge.
-}
getTemporalGraphAt : Int -> TemporalGraph a -> TemporalGraph a
getTemporalGraphAt time tgraph =
    case tgraph of
        TemporalGraph timeline currentTime graph ->
            TemporalGraph timeline time (getGraphAt time tgraph)


{-| Advance the current time of the temporal graph by one
-}
advanceTemporalGraph : TemporalGraph a -> TemporalGraph a
advanceTemporalGraph tgraph =
    case tgraph of
        TemporalGraph timeline currentTime graph ->
            getTemporalGraphAt (Basics.min (List.length timeline) (currentTime + 1)) tgraph


{-| Regress the current time of the temporal graph by one
-}
regressTemporalGraph : TemporalGraph a -> TemporalGraph a
regressTemporalGraph tgraph =
    case tgraph of
        TemporalGraph timeline currentTime graph ->
            getTemporalGraphAt (Basics.max 1 (currentTime - 1)) tgraph


{-| Get the graph of the temporal graph at the provided time
-}
getTgGraph : TemporalGraph a -> Graph a
getTgGraph tgraph =
    case tgraph of
        TemporalGraph timeline currentTime graph -> graph


{-| Get the current graph time
-}
getTemporalGraphTime : TemporalGraph a -> Int
getTemporalGraphTime tgraph =
    case tgraph of
        TemporalGraph timeline currentTime graph -> currentTime


{-| Get the graph timeline
-}
getTemporalGraphTimeline : TemporalGraph a -> List (LengthyEvent a)
getTemporalGraphTimeline tgraph =
    case tgraph of
        TemporalGraph timeline currentTime graph -> timeline
