#Carton

Timeline visualization tool

## Install and Run

### Development
The easiest way to start off is to install `elm` and using its debugging tool
`elm-reactor`. To run the Main.elm file execute the following:

```
git clone https://bitbucket.org/gzakhour/carton.git
cd carton
elm reactor
```

Then go to [http://localhost:8000/src/Main.elm](http://localhost:8000/src/Main.elm).
Press any key on the keyboard to start/stop the animation.
